These scripts set up the Moodle Plugin LaLA (https://github.com/LiFaytheGoblin/moodle-tool_lala/issues) so that it can quickly be tried out.

Use 
* start-local.sh to set up a local web server (accessible on localhost)
* start-server.sh to set up a regular web server (accessible via you fully qualified domain name)

Each script will 
* clone a repo for a docker container set up
* download Moodle 4.2
* download LaLA 3.0-dev
* start the docker container for the Moodle server and database
* install and set up Moodle and LaLA 
* load some test data
* add a cron job to run the adhoc tasks (remove with `crontab -r`)

The admin credentials are:
*username*: `admin`
*password*: `Admin12_`

(If setting up a web server, change these asap!)

## Prerequisites:
* Docker Engine v. 24+
* Docker Compose v. 2+
* Git v. 2+
* A Unix system:
  * start-local.sh was tested on Ubuntu 22
  * start-server.sh was tested on Debian 11
