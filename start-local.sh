#!/bin/bash
set -x

git clone --recurse-submodules https://gitlab.com/iug-research/moodle-learning-analytics/docker-moodle.git
cd docker-moodle
git checkout "moodle-mod-local"

# Select correct Moodle version
cd moodle
git submodule init
git submodule update
git checkout MOODLE_402_STABLE
cd ..

# Set the MOODLE_URL ENV variable to the correct value
echo "MOODLE_URL=http://localhost" >> moodle_variables.env

# Use the correct config for Moodle
cp moodle-config.php moodle/config.php

# Select correct LaLA version
cd moodle-tool_lala
git submodule init
git submodule update
git checkout v3.0-dev
cd ..

# Start up the container
docker compose up -d

sleep 60

# Moodle installation
docker exec -t --user www-data docker-moodle-moodleapp-1 php /var/www/html/admin/cli/install_database.php --agree-license --fullname="test-moodle" --shortname="test-moodle" --adminuser="admin" --adminpass="Admin12_" --adminemail="admin@localhost.de"

# Enable analytics via GUI
docker exec -t --user www-data docker-moodle-moodleapp-1 php /var/www/html/admin/cli/cfg.php --component=analytics --name=onlycli --set=0

# Install LaLA
mkdir moodle/admin/tool/lala/
cp -r "moodle-tool_lala/"* "moodle/admin/tool/lala/"
docker exec -t --user www-data docker-moodle-moodleapp-1 php /var/www/html/admin/cli/upgrade.php --non-interactive

# Restore courses
docker cp mbz/. docker-moodle-moodleapp-1:/var/mbz

for filename in mbz/*; do
	echo "Backing up $filename"
	docker exec -t --user www-data docker-moodle-moodleapp-1 php /var/www/html/admin/cli/restore_backup.php --file=/var/${filename} --categoryid=1
done;

chmod +x run-cron.sh

su
service cron start
update-rc.d cron enable
exit

echo "* * * * * /bin/bash $(pwd)/run-cron.sh >/dev/null 2>&1" | crontab -
